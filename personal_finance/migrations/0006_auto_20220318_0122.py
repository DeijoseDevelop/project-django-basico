# Generated by Django 3.1 on 2022-03-18 01:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('personal_finance', '0005_auto_20220318_0115'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='name_transaction',
            new_name='category_name',
        ),
        migrations.AlterField(
            model_name='transaction',
            name='type',
            field=models.IntegerField(choices=[(0, 'Income'), (1, 'Expenditures')], default=0),
        ),
    ]
