from django.urls import path
from .views import create_transaction, register, application, login_view, create_category, logout_view
from django.contrib.auth.decorators import login_required


app_name = 'personal_finance'

urlpatterns = [
    # url of register form
    path('', register, name='register'),
    # url of login form
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    # url of application for user
    path('application/<str:uuid>/',
         login_required(application, login_url="/login/"), name='application'),
    # url for category
    path('create_category/', login_required(create_category,
         login_url="/login/"), name='create_category'),
    # url for transaction
    path('create_transaction/',
         login_required(create_transaction, login_url="/login/"), name='create_transaction'),
]
