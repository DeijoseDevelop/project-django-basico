from django.apps import AppConfig


class PersonalFinanceConfig(AppConfig):
    """
    configurate the app

    Args:
        AppConfig()
    """
    
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'personal_finance'
