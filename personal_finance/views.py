from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages

from personal_finance.models.transaction import Transaction
from .forms import RegisterForm
from personal_finance.models import User, Category, Transaction
from django.contrib.auth import authenticate, login, logout

# Create your views here.

"""
    if request.user.is_authenticated():
    username = request.user.username
"""


def register(request):
    """_summary_
        this function validates if the form sends a POST method, if the condition is fulfilled it saves the form data in the database.
    it gets the user and the uuid of the user to redirect to the app of that specific user and sends a message if everything is fulfilled successfully. if it is not a POST method it only saves the form and sends it by context.

    Args:
        request: the request object

    Returns:
        render: returns a template of the registry with the forms
    """

    if(request.method == "POST"):
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data["username"]
            user = User.objects.get(username=username)
            uuid = user.uuid
            messages.success(request, f"Account created for {username}!")
            login(request, user)
            return redirect(f'/application/{uuid}')
    else:
        form = RegisterForm()
    context = {"form": form}
    return render(request, 'register.html', context)


def login_view(request):
    """
        this function checks the request is a GET method, if the condition is met it returns the record with an empty context.
if the request is POST it obtains the username and password, uses the authenticate function and saves it in a variable if it is true it uses the login function and redirects to the application of the specific user.

    Args:
        request

    Returns:
        return redirect to appfor user or render to register
    """

    if request.method == 'GET':
        context = ''
        return render(request, 'registration/login.html', {'context': context})
    elif request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect(f'/application/{user.uuid}')
        else:
            context = {}
            messages.error(request, 'Usuario o contraña incorrectos')
            return render(request, 'registration/login.html', {'context': context})


def logout_view(request):
    logout(request)
    return redirect('personal_finance:login')


def application(request, uuid):
    """"
    this function uses the get_object_or_404 function to get a user or a 404, filters the transactions and categories to show the ones specific to that user.

    Keyword arguments: request, uuid of user
    Return: returns a render of the html application and sends the user by context
    """

    all_messages = messages.get_messages(request)
    for _ in all_messages:
        pass
    user = get_object_or_404(User, pk=uuid)
    transactions = Transaction.objects.filter(user_uuid=user.uuid)
    categories = Category.objects.filter(user=user.uuid)
    return render(request, 'application.html', {'user': user, 'transactions': transactions, 'categories': categories})


def create_category(request):
    """
    this function validates if the user is authenticated, if the condition is met it gets the category and creates a new one, then redirects to the same application of the user, if the condition is not met it redirects to the registry.

    Args:
        request: request

    Returns:
        redirect to aplication.html or register.html
    """

    if(request.user.is_authenticated):
        category = request.POST.get('category', '')
        if category and str(category) != "":
            Category.objects.create(category_name=category, user=request.user)
        else:
            messages.error(request, 'Las Categorias no pueden estar vacias')
        return redirect(f'/application/{request.user.uuid}')
    return redirect('personal_finance:login')


def create_transaction(request):
    """sumary_line
        this function validates if the user is authenticated, if it meets the condition it gets the form fields and creates a new transaction, then redirects to the same application of the user, if it does not meet the condition it redirects to the registry. 

    Return: redirection to app or register
    """

    if(request.user.is_authenticated):
        select = request.POST.get('select', '')
        amount = request.POST.get('value-form', '')
        title = request.POST.get('title-form', '')
        category = request.POST.get('select-category', '')

        if select and amount and title and category and str(select) != "" and str(amount) != "" and str(title) != "" and str(category) != "":
            Transaction.objects.create(
                type=select, amount=amount, user_uuid=request.user, category=list(Category.objects.filter(pk=category))[0], name=title)
            messages.success(request, f"añadida transacción {title}")
        else:
            messages.error(request, 'Debe llenar todos los campos')
        return redirect(f'/application/{request.user.uuid}')
    return redirect('personal_finance:login')
