from django.db import models
from .user import User


class Category(models.Model):
    """
        this class create the category with the foreign key of the user

    Args:
        models (_type_): Model
    """

    date = models.DateTimeField(auto_now=True)
    category_name = models.CharField(max_length=50)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.category_name
