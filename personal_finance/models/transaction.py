from django.db import models
from .user import User
from .category import Category


class Transaction(models.Model):
    """
        creates the transaction template with the foreign key of the user_uuid and the category
    """
    class TypeChoice(models.Choices):
        """
        this class handles the types or choices

        Args:
            models: Choices
        """

        income = 0
        expenditures = 1

    name = models.CharField(max_length=50, null=False, blank=False)
    budget_id = models.AutoField(primary_key=True, unique=True)
    type = models.IntegerField(
        choices=TypeChoice.choices, default=TypeChoice.income)

    transaction_date = models.DateField(auto_now=True)
    amount = models.FloatField()

    user_uuid = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, null=True, on_delete=models.CASCADE)
