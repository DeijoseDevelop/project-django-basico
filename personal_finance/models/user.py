import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager as DjangoUserManager


class UserManager(DjangoUserManager):
    """_
        create model UserManager

    Args:
        DjangoUserManager (_type_): Create and save a user with the given username, email, and password.
    """
    def create_user(self, username, type_user="", first_name="", last_name="", email="", password=None):
        return super().create_user(
            username,
            email,
            password,
            type_user=type_user,
            first_name=first_name,
            last_name=last_name,
        )


    def create_superuser(self, username, first_name="", last_name="", email="", password=None):
        return super().create_superuser(
            username,
            email,
            password,
            first_name=first_name,
            last_name=last_name,
        )


class User(AbstractUser):
    """_summary_

    Args:
        AbstractUser (_type_):
            An abstract base class implementing a fully featured User model with
            admin-compliant permissions.

            Username and password are required. Other fields are optional.
    """

    uuid = models.UUIDField(unique=True, default=uuid.uuid4,
                            editable=False, db_index=True, primary_key=True)
    phone = models.CharField(max_length=20, blank=True)

    objects = UserManager()
    USERNAME_FIELD = "username"
