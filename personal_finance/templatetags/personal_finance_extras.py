from django import template
from ..models import Transaction

register = template.Library()


@register.filter
def get_user_owed_total_amount(user):
    """
        :param user:
        return the total amount of the user owed
    """
    total_amount = 0
    transactions = Transaction.objects.filter(user_uuid=user)

    for transaction in transactions:
        if int(transaction.type) == Transaction.TypeChoice.income.value:
            total_amount += transaction.amount
        else:
            total_amount -= transaction.amount
    return total_amount


@register.filter
def calculate_income_total(user):
    """
        :param user:
        return the total amount of the user income
    """
    total_amount = 0
    transactions = Transaction.objects.filter(user_uuid=user)
    for transaction in transactions:
        if int(transaction.type) == Transaction.TypeChoice.income.value:
            total_amount += transaction.amount
    return total_amount


@register.filter
def calculate_expense_total(user):
    """
        :param user:
        return the total amount of the user expense
    """
    total_amount = 0
    transactions = Transaction.objects.filter(user_uuid=user)
    for transaction in transactions:
        if int(transaction.type) == Transaction.TypeChoice.expenditures.value:
            total_amount += transaction.amount
    return total_amount
