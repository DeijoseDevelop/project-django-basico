from django.contrib.admin import ModelAdmin, register
from .models.category import Category
from .models.transaction import Transaction
from .models.user import User
# Register your models here.

# register User model
@register(User)
class UserAdmin(ModelAdmin):
    list_display = ("uuid", 'username', 'email', 'phone',)

# register category model
@register(Category)
class CategoryAdmin(ModelAdmin):
    list_display = ('id', 'category_name', 'date')

# register transaction model
@register(Transaction)
class TransactionAdmin(ModelAdmin):
    list_display = ('budget_id', 'name', 'type', 'amount', 'transaction_date')
